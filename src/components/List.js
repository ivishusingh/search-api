import React, { Component } from "react";
import Navbar from "./Navbar";
import axios from "axios";

export default class List extends Component {
  state = {
    list: [],
    searchInput: ""
  };
  componentDidMount() {
    axios
      .get("https://api.myjson.com/bins/1dlper")
      .then(res => res.data)
      .then(data => this.setState({ list: data }));
  }

  render() {
    let dat = Object.values(this.state.list);

    let roll = dat.map((ent, i) => {
      return (
        <tr key={i}>
          <td>{ent.rollNo}</td>
          <td>{ent.name}</td>
          <td>{ent.class}</td>
          <td>{ent.marks.s1}</td>
        </tr>
      );
    });
    return (
      <div>
        <Navbar dat={dat} />
        <div
          style={{ backgroundColor: "	#228B22" }}
          className="jumbotron jumbotron-fluid"
        >
          <div className="container">
            <div className="row">
              <div className="col">
                <input
                  className="form-control mb-2 mr-sm-2"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                  onChange={e => {
                    this.setState({ searchInput: e.target.value });
                  }}
                />
                <button
                  className=" mt-2 btn btn-dark my-2 my-sm-0"
                  type="submit"
                  onClick={e => {
                    e.preventDefault();
                    const data = dat.filter(item =>
                      item.name
                        .toLowerCase()
                        .includes(this.state.searchInput.toLowerCase())
                    );
                    this.setState({ list: data });
                  }}
                >
                  Search
                </button>

                <button type="button" className="btn btn-dark m-2">
                  sort
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <table className="table table-striped">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Roll no</th>
                  <th scope="col">Name</th>
                  <th scope="col">Class</th>
                  <th scope="col" colSpan="2">
                    Marks
                  </th>
                </tr>
              </thead>

              <tbody>{roll}</tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
