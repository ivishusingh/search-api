import React from "react";

// import {NavLink} from 'react-router-dom'

export default class Navbar extends React.Component {
  render() {
    return (
      <div>
        <div>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <h4 className="navbar-brand" href="#">
              API-FETCh
            </h4>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item ">
                  <h6 className="nav-link">
                    Home <span className="sr-only">(current)</span>
                  </h6>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}
