import React, { Component } from "react";
import List from "./components/List";
import "./App.css";
import { BrowserRouter, Route, Switch, Router } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <List />
        {/* <BrowserRouter>
          <Switch>
            <Router>
              <Route exact path="/" component={List} />
            </Router>
          </Switch>
        </BrowserRouter> */}
      </div>
    );
  }
}

export default App;
